import Head from 'next/head'
import { Button } from '@material-ui/core';
import styles from './Index.module.css'

export default function Home() {
  return (
    <div className="container">
      <main>
        <h1 className={styles.title}> 
        "Welcome to the Davies Family Quiz"
        </h1>
        <p className="description">
          <Button 
            className={styles.startButton}
            color="primary"
            size="large"
            style={{fontFamily:"sans-serif"}}
            variant="contained" >Let's get started!</Button>
        </p>
      </main>
    </div>
  )
}
